#!/bin/bash

POOL=stratum+tcp://cn.sparkpool.com:3335
WALLET=0x729fc14f6f6464891cdb8f70d093bb58815b21b8
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )

cd "$(dirname "$0")"

chmod +x ./gasken && ./gasken --algo ETHASH --pool $POOL --user $WALLET $@ --4g-alloc-size 4076
